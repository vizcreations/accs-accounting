/**
* @entry source file with functions for accs-accounting
* @author VizCreations
*
*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "accs.h"
#include "type.h"
#include "acc.h"
#include "entry.h"

void add_entr_(ENTR (*entry)[], ACC (*acc)[], int id, int acc_id, char *key, char *val, short cr, short dr) {
	FILE *fp;
	time_t ticks;
	ENTR *entr;
	ACC *acct;
	TYP *type;
	int typeid = 0;
	int value;
	int kkey = -1;
	int acckey = -1;
	ticks = time(NULL);
	typeid = atoi((*acc)[acc_id].type_id);
	value = atoi(val);
	kkey = id - 1;
	acckey = acc_id-1;
	fp = fopen(ENTR_FILE, "a+");
	if(fp) {
		fprintf(fp, "%d,%d,%d,%d,%d,%s,%s,%d,%.24s\n",
			id, acc_id, 0, cr, dr, key, val, 1, ctime(&ticks));
		fclose(fp);
	}
	/** Now propagate to structure memory for show-acc */
	// We need key which is nothing but the ID;
	entr = &(*entry)[kkey];
	sprintf(entr->id, "%d", id);
	sprintf(entr->acc_id, "%d", acc_id);
	strcpy(entr->comp_id, "0");
	entr->cr = cr;
	entr->dr = dr;
	strcpy(entr->name, key);
	entr->value = atoi(val);
	sprintf(entr->timestamp, "%.24s", ctime(&ticks));

	// Now update account data
	acct = &(*acc)[acckey];
	/*if(typeid == 1 && dr == 1) // Increase
		acct->balance += value;
	else if(typeid == 2 && dr == 1) // Decrease
		acct->balance -= value;
	else if(typeid == 1 && cr == 1) // Decrease
		acct->balance -= value;
	else if(typeid == 2 && cr == 1) // Increase
		acct->balance += value;*/
/*
	if(acc_id > 0 && acc_id < 9) { // Assets that increase on debit and decrease on credit
		if(cr==1&&dr==0) {
			acct->balance -= value;
		} else if(cr==0&&dr==1) {
			acct->balance += value;
		}
	} else if(acc_id > 8 && acc_id < 14) { // Liability accounts that increase on credit and decrease on debit
		if(cr==1 && dr==0) {
			acct->balance += value;
		} else if(cr==0 && dr==1) {
			acct->balance -= value;
		}
	} else if(acc_id==14) { // Owner equity accounts that increase on credit and decrease on debit
		if(cr==1 && dr==0) {
			acct->balance += value;
		} else if(cr==0 && dr==1) {
			acct->balance -= value;
		}
	} else if(acc_id==15) { // Owner equity accounts that increase on debit and decrease on credit
		if(cr==1 && dr==0) {
			acct->balance -= value;
		} else if(cr==0 && dr==1) {
			acct->balance += value;
		}
	} else if(acc_id==16) { // Revenue accounts that increase on credit and decrease on debit
		if(cr==1&&dr==0) {
			acct->balance += value;
		} else if(cr==0&&dr==1) {
			acct->balance -= value;
		}
	} else if(acc_id>16 && acc_id<24) { // Expense accounts that increase on debit and decrease on credit
		if(cr==1 && dr==0) {
			acct->balance -= value;
		} else if(cr==0 && dr==1) {
			acct->balance += value;
		}
	} else if(acc_id==24) { // Non operating revenue accounts that increase on credit and decrease on debit
		if(cr==1 && dr==0) {
			acct->balance += value;
		} else if(cr==0 && dr==1) {
			acct->balance -= value;
		}
	} else if(acc_id==25) { // Profit accounts that increase on credit and decrease on debit
		if(cr==1 && dr==0) {
			acct->balance += value;
		} else if(cr==0 && dr==1) {
			acct->balance -= value;
		}
	} else if(acc_id==26) { // Loss accounts that increase on debit and decrease on credit
		if(cr==1 && dr==0) {
			acct->balance -= value;
		} else if(cr==0 && dr==1) {
			acct->balance += value;
		}
	} // This whole this must be programmed
*/
	if(acct->cr == 1 && acct->dr == 0) { // Credit balance
		if(cr==1 && dr==0) {
			acct->balance += value;
		} else if(cr==0 && dr==1) {
			acct->balance -= value;
		}
	} else if(acct->cr == 0 && acct->dr == 1) { // Debit balance
		if(cr==1 && dr ==0) {
			acct->balance -= value;
		} else if(cr==0 && dr==1) {
			acct->balance += value;
		}
	}
}

void del_entr(ENTR (*entry)[], int id) {
	// TODO CODE
}

void show_entr_(ENTR (*entry)[], ACC (*acc)[], int entr_count) {
	int i = 0, j = 0;
	ENTR *entr;
	ACC *acct;
	int accid = 0;
	long left = 0;
	long right = 0;
	if(entr_count<1) {
		printf("No entries created..\n");
	} else {
		puts("+--------------------------+--------------------------+----------------+----------------+");
		/*printf("| %-9s| %-7s| %-5s| %-20s| %-13s |\n",
				"BILL-ID", "ITEMS", "QTY", "CUSTOMER", "PRICE(+TAX)");
		puts("+----------+--------+------+---------------------+---------------+");*/
		printf("| %-25s| %-25s| %-15s| %-15s|\n",
			"Date", "Account", "Debit", "Credit");
		puts("+--------------------------+--------------------------+----------------+----------------+");
		//printf("Name-Value\t\tName-Value");
		for(i=0; i<entr_count; i++) {
			entr = &(*entry)[i];
			accid = atoi(entr->acc_id);
			acct = &(*acc)[accid-1];
			if(entr->dr==1) {
				/*printf("%s\t%s\t%s\t%d\t%d\t%d\n",
					entr->id, acct->name, entr->name, entr->cr, entr->dr, entr->value);*/
				printf("| %-25s| %-25s| %-15d| %-15d|\n",
					entr->timestamp, acct->name, entr->value, 0);
				left += entr->value;
			} else if(entr->cr==1) {
				/*printf("%s\t%s\t%s\t%d\t%d\t%d\n",
					entr->id, acct->name, entr->name, entr->cr, entr->dr, entr->value);*/
				printf("| %-25s| %-25s| %-15d| %-15d|\n",
					entr->timestamp, acct->name, 0, entr->value);
				right += entr->value;
			}
		}
		puts("+--------------------------+--------------------------+----------------+----------------+");
		printf("| %-25s| %-25s| %-15ld| %-15ld|\n",
			"", "Total", left, right);
		puts("+--------------------------+--------------------------+----------------+----------------+");
	}
}

void save_entr_(ENTR (*entr)[], int entr_count) {
	int i = 0;
	ENTR *entry;
	FILE *fp;
	if(entr_count > 0) {
		fp = fopen(ENTR_FILE, "w");
		if(fp) {
			for(i=0; i<entr_count; i++) {
				entry = &(*entr)[i];
				fprintf(fp, "%s,%s,%d,%d,%d,%s,%d,%d,%s\n",
					entry->id, entry->acc_id, 0, entry->cr, entry->dr, entry->name, entry->value, 1, entry->timestamp);
			}
			fclose(fp);
		}
	}
}
