/**
* @company header file for accs-accounting
* @author VizCreations
*
*/

/** This file is part of AccS.

*    AccS is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    AccS is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with AccS.  If not, see <http://www.gnu.org/licenses/>.
*/

#define COMP_FILE "companies.csv"
#define MAX_COMP_ID 10
#define MAX_COMP_NAME 100
#define MAX_COMP_LEN 255
#define MAX_COMP 5000

typedef struct Company {
	char id[10];
	char name[MAX_COMP_NAME];
	char desc[MAX_COMP_LEN];
	char estdate[MAX_COMP_LEN];
	char addr[MAX_COMP_LEN];
	char timestamp[MAX_COMP_LEN];
	int count;
	int cur_id;
} CMP;

void add_comp_(CMP *cmp, char *, int);
void show_comps_(CMP *cmp);
void save_comps_(CMP *cmp);
