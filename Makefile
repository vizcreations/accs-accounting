# @accounting Makefile
# @author VizCreations
#

SRC_CIL = accs.c license.c comp.c entry.c invt.c init.c acc.c strfun.c type.c
OBJ_CIL = accs.o license.o comp.o entry.o invt.o init.o acc.o strfun.o type.o
CGI_BIN = /home/vizcreations/www/cgi-bin/

CIL_INCLUDES = -I/usr/include -I. -I/usr/local/include -I/usr/local/mysql/include
CIL_LIBS = -L/usr/lib -L/usr/local/lib -L/usr/local/mysql/lib -lmysqlclient

all: cil compile
cil:
	gcc -c $(SRC_CIL)
	ar rcs accs.a $(OBJ_CIL)
	$(RM) *.o
compile:
	gcc -o accs $(CIL_INCLUDES) main.c accs.a $(CIL_LIBS) -lc
install:
	cp accs $(CGI_BIN)
clean:
	$(RM) accs ACCS AccS *.csv *.a
