/**
* @common functions source file with strings
* @author VizCreations
*
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void strip_newline(char *str, int len) {
	int i = 0;
	for(i=0; i<len; i++) {
		if(str[i] == '\n') {
			str[i] = '\0';
			break;
		}
	}
}

char *trim(char *str, int len) {
	char *newstr = malloc(1);
	int newlen = 0;
	char *trimmed;
	int i = 0, j = 0, k = 0;
	newstr = realloc(newstr, len);
	
	while(str[i] == ' ') // Skip spaces at the beginning
		++i;

	while(str[i] == '\t') // Skip tabs too
		++i;

	while(newstr[j++] = str[i]) // Creating new string
		++i;

	newstr[j] = '\0';
	newlen = j;

	/** Now try removing trailing spaces */
	while((newstr[newlen] == ' '))
		newlen--;

	while((newstr[newlen] == '\t')) // Remove tabs too
		newlen--;

	newstr[newlen] = '\0';
	strip_newline(newstr, newlen);
	//strcpy(trimmed, newstr);

	return newstr;
}
