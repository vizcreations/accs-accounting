/**
* @account-type source file with functions for accs-accounting
* @author VizCreations
*
*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "type.h"

void add_type_(TYP (*type)[], int id, char *val) {
	FILE *fp;
	time_t ticks;
	ticks = time(NULL);
	fp = fopen(TYPE_FILE, "a+");
	TYP *typ;
	strip_newline(val, MAX_TYPE_INPUT);
	if(fp) {
		fprintf(fp, "%d,%s,%s,%d,%.24s\n",
			id, val, "N/A", 1, ctime(&ticks));
		fclose(fp);
	}
	/** Now propagate to structure memory for show-acc */
	// We need key which is nothing but the ID;
	typ = &(*type)[id];
	sprintf(typ->id, "%d", id);
	strcpy(typ->name, val);
	strcpy(typ->code, "1");
	sprintf(typ->timestamp, "%.24s", ctime(&ticks));
}

void del_type_(TYP (*type)[], int id) {
	// TODO CODE
}

void show_types_(TYP (*type)[], int type_count) {
	int i = 0, j = 0;
	TYP *typ;
	if(type_count<1) {
		printf("No types created..\n");
	} else {
		puts("+----------+--------------------------+----------+--------------------------+");
		printf("| %-9s| %-25s| %-9s| %-25s|\n",
				"ID", "Name", "Code", "Time");
		puts("+----------+--------------------------+----------+--------------------------+");
		for(i=0; i<type_count; i++) {
			typ = &(*type)[i];
			printf("| %-9s| %-25s| %-9s| %-25s|\n",
				typ->id, typ->name, typ->code, typ->timestamp);
		}
		puts("+----------+--------------------------+----------+--------------------------+");
	}
}
