/**
* @init source functions for accs-accounting
* @author VizCreations
*
*/

#include <stdio.h>
#include <string.h>
//#include "comp.h"
#include "type.h"
#include "acc.h"
#include "entry.h"
#include "strfun.h"

/*void init_comps__(CMP *cmp) {
	// TODO CODE
}
*/

int scan_type_file_(TYP (*type)[], FILE *fp) {
	char *token = NULL;
	int lines = 0;
	char line[MAX_TYPE_LINE];
	char delim[] = ",";
	int key = 0;
	char id[MIN_TYPE_INPUT];
	char name[MAX_TYPE_INPUT];
	char desc[MAX_TYPE_STR];
	char code[MIN_TYPE_INPUT];
	char timestamp[MAX_TYPE_INPUT];
	TYP *typ;
	if(fp) {
		while(fgets(line, MAX_TYPE_LINE, fp) != NULL) {
			typ = &(*type)[key];
			if((token=strtok(line, delim))!=NULL) { // id
				strcpy(typ->id, trim(token, MIN_TYPE_INPUT));
				if((token=strtok(NULL, delim))!=NULL) { // name
					strcpy(typ->name, trim(token, MAX_TYPE_INPUT));
					if((token=strtok(NULL, delim))!=NULL) {
						strcpy(typ->desc, trim(token, MAX_TYPE_STR));
						if((token=strtok(NULL,delim))!=NULL) { //code
							strcpy(typ->code, trim(token, MIN_TYPE_INPUT));
							if((token = strtok(NULL, delim)) != NULL) { // timestamp
								strcpy(typ->timestamp, trim(token, MAX_TYPE_INPUT));
								strip_newline(typ->timestamp, MAX_TYPE_INPUT);
							}
						}
					}
				}
			}
			++lines; ++key;
		}
	}
	return lines;
}

int scan_acc_file_(ACC (*acc)[], FILE *fp) { // Scan using tokens
	char *token = NULL;
	int lines = 0;
	char line[MAX_ACC_LINE];
	char delim[] = ",";
	int key = 0;
	char id[MIN_ACC_INPUT];
	char name[MAX_ACC_INPUT];
	short cr, dr;
	char desc[MAX_ACC_STR];
	char code[10];
	char timestamp[MIN_ACC_INPUT];
	ACC *acct;
	if(fp) {
		while(fgets(line, MAX_ACC_LINE, fp) != NULL) {
			acct = &(*acc)[key];
			/*id = acct->id;
			name = acct->name;
			desc = acct->desc;
			code = acct->code;
			cr = acct->cr;
			dr = acct->dr;*/

			if((token = strtok(line, delim)) != NULL) {
				//(*acc)[key].id[0] = '\0';
				//printf("STRUCT SIZE: '%d'\n", sizeof(acc));
				//strcpy((*acc)[key].id, token); break;// acc is already pointing to first item
				strcpy(id, trim(token, MIN_ACC_INPUT));
				acct->id[0] = 0;
				strcpy(acct->id, id);
				if((token = strtok(NULL, delim)) != NULL) { // Type
					strcpy(acct->type_id, trim(token, MIN_ACC_INPUT));
					if((token = strtok(NULL, delim)) != NULL) { // Name
						strcpy(acct->name, trim(token, MAX_ACC_INPUT));
						if((token=strtok(NULL, delim)) != NULL) { // Balance
							acct->balance = atoi(trim(token, MAX_ACC_INPUT));
							if((token=strtok(NULL, delim)) != NULL) { // Cr
								acct->cr = atoi(trim(token, MIN_ACC_INPUT));
								if((token=strtok(NULL, delim)) != NULL) { // Dr
									acct->dr = atoi(trim(token, MIN_ACC_INPUT));
									if((token=strtok(NULL, delim)) != NULL) { // Code
										strcpy(acct->code, trim(token, MIN_ACC_INPUT));
										if((token=strtok(NULL, delim)) != NULL) { // Time
											strcpy(acct->timestamp, trim(token, MAX_ACC_INPUT));
											strip_newline(acct->timestamp, MAX_ACC_INPUT);
										}
									}
								}
							}
						}
					}
				}

			}
			++lines; ++key;
		}
	}

	return lines;
}

int scan_entr_file_(ENTR (*entry)[], FILE *fp) { // Scan using tokens
	char *token = NULL;
	int lines = 0;
	char line[MAX_ENTR_LINE];
	char delim[] = ",";
	int key = 0;
	char id[MIN_ENTR_INPUT];
	char acc_id[MIN_ENTR_INPUT];
	char name[MAX_ENTR_INPUT];
	char value[MAX_ENTR_INPUT];
	short cr, dr;
	char desc[MAX_ENTR_STR];
	char code[10];
	char timestamp[MIN_ENTR_INPUT];
	ENTR *entr;
	if(fp) {
		while(fgets(line, MAX_ENTR_LINE, fp) != NULL) {
			entr = &(*entry)[key];
			/*id = acct->id;
			name = acct->name;
			desc = acct->desc;
			code = acct->code;
			cr = acct->cr;
			dr = acct->dr;*/

			if((token = strtok(line, delim)) != NULL) { // Entry ID
				//(*acc)[key].id[0] = '\0';
				//printf("STRUCT SIZE: '%d'\n", sizeof(acc));
				//strcpy((*acc)[key].id, token); break;// acc is already pointing to first item
				strcpy(id, trim(token, MIN_ENTR_INPUT));
				entr->id[0] = 0;
				strcpy(entr->id, id);
				if((token = strtok(NULL, delim)) != NULL) { // This is for acc-id
					//strcpy((*acc)[key].name, token);
					strcpy(entr->acc_id, trim(token, MIN_ENTR_INPUT));
					if((token=strtok(NULL, delim))!=NULL) { // This is comp-id
						strcpy(entr->comp_id, trim(token, MIN_ENTR_INPUT));
						if((token = strtok(NULL, delim)) != NULL) { // Cr
							entr->cr = atoi(trim(token, MIN_ENTR_INPUT));
							if((token=strtok(NULL, delim)) != NULL) { // Dr
								entr->dr = atoi(trim(token, MIN_ENTR_INPUT));
								if((token=strtok(NULL, delim)) != NULL) { // Name
									strcpy(entr->name, trim(token, MAX_ENTR_INPUT));
									if((token=strtok(NULL, delim)) != NULL) { //Value
										entr->value = atoi(trim(token, MAX_ENTR_INPUT));
										if((token=strtok(NULL, delim)) != NULL) { // Code
											strcpy(entr->code, trim(token, MIN_ENTR_INPUT));
											if((token=strtok(NULL, delim))!=NULL) {
												strcpy(entr->timestamp, trim(token, MAX_ENTR_INPUT));
												strip_newline(entr->timestamp, MAX_ENTR_INPUT);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			++lines; ++key;
		}
	}

	return lines;
}

int init_type__(TYP (*type)[]) {
	FILE *fp;
	int type_count = 0;
	fp = fopen(TYPE_FILE, "r");
	if(fp) {
		type_count = scan_type_file_(type, fp);
		fclose(fp);
	}
	return type_count;
}

int init_acc__(ACC (*acc)[]) {
	/** Load the file if present. Each line is a record */
	FILE *fp;
	int acc_count = 0;
	fp = fopen(ACC_FILE, "r");
	if(fp) { // File exists, load data
		acc_count = scan_acc_file_(acc, fp);
		fclose(fp);
	}
	return acc_count;
}

int init_entr__(ENTR (*entr)[]) {
	FILE *fp;
	int entr_count = 0;
	fp = fopen(ENTR_FILE, "r");
	if(fp) {
		entr_count = scan_entr_file_(entr, fp);
		fclose(fp);
	}
	return entr_count; // Always 1 more than actual records
}
