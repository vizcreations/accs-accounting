AccS - Accounting Solution
==========================
ACCS is an accounting program for the Linux shell.


Is it free to use?
==================
The program is open-source. So, yes it's free


How to install?
===============
As a linux user, you should know how to install from source

1. make all
2. make install (or)
3. Simply unzip from zipped version and extract to /usr/local
directory which is similar to #2

When you make install, the directory where you're accessing
the program from is the directory where data gets saved.


How to use?
===========
Usage manual as follows for basic purposes

1. :~$ ./accs type -n "Asset" // Creates new account type
2. :~$ ./accs acc -n 200 dr "Note payable" // New account under type assuming 200 is ID of liability and debit balance
3. :~$ ./accs entr -n dr 100 "Got loan" 50000 cr 200 "loan to be paid" 50000 // Basic double-entry to cash and payable
4. :~$ ./accs bals -s // Display balance sheet
5. :~$ ./accs acc -s // Display accounts
6. :~$ ./accs type -s // Display account types
7. :~$ ./accs entr -s // Show all entries in Journal-esque format


Preloaded data and accounts
===========================
The program comes pre-loaded with basic account types
to get you started with simply adding entries and
checking balance and income sheets.


Is it robust and fully professional?
====================================
Sadly no. It's a fun project and has problems. Well,
free stuff don't guarantee you anything. :-)


I am a programmer, I want to enhance it
=======================================
You are welcome to do this. Clone or fork the project here
https://bitbucket.org/vizcreations/accs-accounting

The program is written in C and will be supporting only C.


~VizCreations
