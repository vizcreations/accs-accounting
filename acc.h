/**
* @account header file for accs-accounting
* @author VizCreations
*
*/

#define MAX_ACC_INPUT 254
#define MIN_ACC_INPUT 51
#define MAX_ACC_STR 5000
#define MAX_ACC_LINE 290
#define MAX_ACC 500
#define CURR '$'

#define ACC_FILE "accounts.csv"

typedef struct Account {
	char id[MIN_ACC_INPUT];
	char type_id[MAX_ACC_INPUT];
	int comp_id;
	char name[MAX_ACC_INPUT];
	char desc[MAX_ACC_STR];
	long balance;
	short cr;
	short dr;
	char code[10];
	char timestamp[MAX_ACC_INPUT];
} ACC;

void add_acc_(ACC (*acc)[], int, int, char *);
void del_acc_(ACC (*acc)[], int);
void show_acc_(ACC (*acc)[], TYP (*type)[], int);
void save_acc_(ACC (*acc)[], int);
void show_bals_(ACC (*acc)[], TYP (*type)[], int);
void show_incs_(ACC (*acc)[], TYP (*type)[], int);
