/**
* @config header file for common string functions
* @author VizCreations
* @declarations for macros and structures
*/

#define MAX_STR_INPUT 255
#define MIN_STR_INPUT 10
#define MAX_STR_LEN 2000
#define EXTRA_STR 5
#define MAX_STR_LINE MAX_STR+EXTRA
#define MAX_STR_RECORDS 100
#define MAX_STR_DATA 5000
#define MAX_STR_FILEPATH 200
#define MAX_STR_CMD 100

void strip_newline(char *, int);
char *trim(char *, int);
