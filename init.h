/**
* @accs header file for accs-accounting
* @author VizCreations
*
*/

#define MAX_TRIAL 50

//void init_comps__(CMP *comp);
int scan_type_file_(TYP (*type)[], FILE *);
int scan_acc_file_(ACC (*acc)[], FILE *);
int scan_entr_file_(ENTR (*entr)[], FILE *);
int init_type__(TYP (*type)[]);
int init_acc__(ACC (*acc)[]);
int init_entr__(ENTR (*entr)[]);
