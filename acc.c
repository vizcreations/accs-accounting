/**
* @accounts source file with functions for accs-accounting
* @author VizCreations
*
*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "type.h"
#include "acc.h"

void add_acc(ACC (*acc)[], int typeid, int id, char *name, short cr, short dr) {
	FILE *fp;
	time_t ticks;
	int key = -1;
	ticks = time(NULL);
	fp = fopen(ACC_FILE, "a+");
	ACC *acct;
	key = id-1;
	if(fp) {
		fprintf(fp, "%d,%d,%s,%d,%d,%d,%d,%.24s\n",
			id, typeid, name, 0, cr, dr, 1, ctime(&ticks));
		fclose(fp);
	}
	/** Now propagate to structure memory for show-acc */
	// We need key which is nothing but the ID;
	acct = &(*acc)[key];
	sprintf(acct->id, "%d", id);
	sprintf(acct->type_id, "%d", typeid);
	strcpy(acct->name, name);
	acct->balance = 0;
	acct->cr = cr;
	acct->dr = dr;
	strcpy(acct->code, "1");
	sprintf(acct->timestamp, "%.24s", ctime(&ticks));
}

void del_acc(ACC (*acc)[], int id) {
	// TODO CODE
}

void show_acc_(ACC (*acc)[], TYP (*type)[], int acc_count) {
	int i = 0, j = 0;
	ACC *acct;
	TYP *typ;
	int typeid = 0;
	char *bal;
	if(acc_count<1) {
		printf("No accounts created..\n");
	} else {
		puts("+----------+--------------------------+---------------------+----------------+-----------+");
		printf("| %-9s| %-25s| %-20s| %-15s| %-10s|\n",
				"ID", "Name", "Balance", "Type", "Cr/Dr");
		puts("+----------+--------------------------+---------------------+----------------+-----------+");
		for(i=0; i<acc_count; i++) {
			acct = &(*acc)[i];
			typeid = atoi(acct->type_id);
			typ = &(*type)[typeid-1];
			if(acct->cr == 0 && acct->dr == 1) bal = "Debit";
			else if(acct->cr == 1 && acct->dr == 0) bal = "Credit";
			printf("| %-9s| %-25s| %-20ld| %-15s| %-10s|\n",
				acct->id, acct->name, acct->balance, typ->name, bal);
		}
		puts("+----------+--------------------------+---------------------+----------------+-----------+");
	}
}

void save_acc_(ACC (*acc)[], int acc_count) {
	int j = 0;
	ACC *acct;
	FILE *fp;
	if(acc_count > 0) {
		fp = fopen(ACC_FILE, "w");
		if(fp) {
			for(j=0; j<acc_count; j++) {
				acct = &(*acc)[j];
				fprintf(fp, "%s,%s,%s,%ld,%d,%d,%d,%s\n",
					acct->id, acct->type_id, acct->name, acct->balance, acct->cr, acct->dr, 1, acct->timestamp);
			}
			fclose(fp);
		}
	}
}

void show_bals_(ACC (*acc)[], TYP (*type)[], int acc_count) {
	int i = 0, j = 0;
	ACC *acct;
	TYP *typ;
	int typeid = 0;
	int accid = 0;
	long left = 0;
	long right = 0;
	if(acc_count<1) {
		printf("No accounts created..\n");
	} else {
		// Left we show asset accounts
		puts("+------------------------------------+---------------------------------------------------+");
		printf("| %-35s| %-50s|\n",
				"Assets", "Liabilities + Equity + Revenue + Profit - Loss");
		puts("+------------------------------------+---------------------------------------------------+");
		for(i=0; i<acc_count; i++) {
			acct = &(*acc)[i];
			typeid = atoi(acct->type_id);
			if(typeid < 2) { // It's an asset
				printf("| %-25s%-10ld| %-50c|\n",
					acct->name, acct->balance, '-');
				left += acct->balance;
			} else {
				printf("| %-35c| %-35s%-15ld|\n",
					'-', acct->name, acct->balance);
				right += acct->balance;
			}
		}
		puts("+------------------------------------+---------------------------------------------------+");
		printf("| %-25s%-10ld| %-35s%-15ld|\n",
				"Total", left, "Total", right);
		puts("+------------------------------------+---------------------------------------------------+");
	}
}

void show_incs_(ACC (*acc)[], TYP (*type)[], int acc_count) {
	// Use revenue account balances and subtract from expense balances
	int i = 0;
	ACC *acct;
	TYP *typ;
	int rvid = 4; // For revenue
	int expid = 5;
	long revenue = 0;
	long expense = 0;
	if(acc_count<1) {
		printf("No accounts created..\n");
	} else {
		// First revenue, then expenses
		puts("+--------------------------+-----------+");
		printf("| %-25s  %-10s|\n",
			"Revenues", "");
		puts("+--------------------------+-----------+");
		for(i=0; i<acc_count; i++) {
			acct = &(*acc)[i];
			if(atoi(acct->type_id) == rvid) { // Revenue account
				printf("| %-25s| %-10ld|\n",
					acct->name, acct->balance);
				revenue += acct->balance;
			}
		}
		puts("+--------------------------+-----------+");
		i = 0;
		printf("| %-25s  %-10s|\n",
			"Expenses", "");
		puts("+--------------------------+-----------+");
		for(i=0; i<acc_count; i++) {
			acct = &(*acc)[i];
			if(atoi(acct->type_id) == expid) { // Expense account
				printf("| %-25s| %-10ld|\n",
					acct->name, acct->balance);
				expense += acct->balance;
			}
		}
		puts("+--------------------------+-----------+");
		printf("| %-25s| %-10ld|\n",
			"Total", (revenue - expense));
		puts("+--------------------------+-----------+");
	}
}
