/**
* @company functions source file for accs-accounting
* @author VizCreations
*
*
*/

/** This file is part of AccS.

*    AccS is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    AccS is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with AccS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp.h"

void add_comp_(CMP *comp, char *val, int len) {
	// TODO CODE
}

void show_comps_(CMP *comp) {
	// TODO CODE
}

void save_comps_(CMP *comp) {
	// TODO CODE
}
