/**
* @license display source for accs-accounting
* @author VizCreations
*
*/

/** This file is part of AccS.

*    AccS is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    AccS is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with AccS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>

void show_license_(void) {
	char str[] = "AccS  Copyright (C) 2014  VizCreations\n"
			"This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.\n"
			"This is free software, and you are welcome to redistribute it\n"
			"under certain conditions; type `show c' for details.\n";
	printf("%s", str);
}
