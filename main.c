/**
* @main source file for AccS accounting
* @author VizCreations
* @abstract main source entry file for program
*
*/

/** This file is part of AccS.

*    AccS is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    AccS is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with AccS.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "accs.h"
#include "license.h"
//#include "comp.h"
#include "type.h"
#include "acc.h"
#include "entry.h"
#include "invt.h"
#include "init.h"
#include "strfun.h"

int main(int argc, char *argv[], char *env[]) {
	char op;
	char *ent;
	boolean cr = FALSE;
	boolean dr = FALSE;
	double entry = 0.0;
	int comp_count=0, type_count=0, acc_count=0, entr_count=0;
	FILE *fp;

	//CMP cmp;
	TYP type[MAX_TYPE];
	ACC acc[MAX_ACC];
	ENTR entr[MAX_ENTR];
	int cur_comp = FALSE; // If it's FALSE, we don't allow any action
	int cur_acc = FALSE;
	int entryid = 0;
	int accid = 0;
	int typeid = 0;

	int id = 0;
	char *key;
	char *val;
	time_t ticks;

	show_license_();
	if(argc < 2) {
		printf("Usage: %s [entity] [option]\n", argv[0]);
		return -1;
	}

	/** Initialize data from files */
	//init_comp__(&cmp[0]);
	type_count = init_type__(&type);
	acc_count = init_acc__(&acc);
	entr_count = init_entr__(&entr);

	ent = argv[1]; // Entity
	ticks = time(NULL);
	if(argc>=2) {
		if(TRUE) {
			if(argc>2) {
				if(argv[2][0] == '-')
					op = argv[2][1];
				else
					op = 0;
			}

			if(strcmp(ent, "comp") == 0) { // Selected company
				printf("Coming soon.\n"); return 1;
				if(op!=0 && argc>3) {
					comp_count = 0;
					id = comp_count; // Total is always 1 more than actual data
					val = argv[3];
					switch(op) {
						case 'n': // New company
							{
								fp = fopen(COMPFILE, "a+");
								if(fp) {
									fprintf(fp, "%d, %s, %s, %s, %s, %s\n",
										id, val, "N/A", "N/A", "N/A", ctime(&ticks));
									fclose(fp);
								}
								//save_comps_(&cmp[0]);
							}
							break;
						case 'u':
							break;
						case 'l': // Show companies
							break;
					}
				}
				//show_comps_(cmp);
			} else if(strcmp(ent, "type") == 0) { // Account type
				if(op!=0 && argc>3) {
					if(type_count==0) type_count = 1;
					else ++type_count;
					id = type_count;
					++type_count;
					val = argv[3];
					switch(op) {
						case 'n': // New type
							{
								add_type_(&type, id, val);
							}
							break;
						case 'u':
							break;
					}
				}
				show_types_(&type, type_count);
			} else if(strcmp(ent, "acc") == 0) { // Account
				if(op!=0 && argc>5) {
					if(acc_count == 0) acc_count = 1;
					else ++acc_count; // For ID
					id = acc_count;
					//++acc_count; // Already create the trailing last number which isn't a key
					type_count = atoi(argv[3]); // The type-id (Eg: Asset or Revenue)
					val = argv[5];
					strip_newline(val, MAX_ACC_INPUT);
					switch(op) {
						case 'n': // New account
							{
								if(strcmp(argv[4], "cr") == 0) { // Credit balance
									cr = TRUE; dr = FALSE;
								} else if(strcmp(argv[4], "dr") == 0) { // Debit balance
									cr = FALSE; dr = TRUE;
								}
								if(cr == FALSE) cr = 0;
								else if(dr == FALSE) dr = 0;
								add_acc(&acc, type_count, id, val, cr, dr);
							}
							break;
						case 'u':
							break;
					}
				}
				show_acc_(&acc, &type, acc_count);
			} else if(strcmp(ent, "entr") == 0) { // Entry
				if(op!=0 && argc>6) {
					if(entr_count == 0) entr_count = 1;
					else ++entr_count;
					id = entr_count;
					if(argc>6) {
						accid = atoi(argv[4]); // acc_count = acc_id
						key = argv[5];
						val = argv[6];
						if(strcmp(argv[3], "cr") == 0) {
							cr = TRUE; dr=FALSE;
						} else if(strcmp(argv[3], "dr") == 0) {
							cr = FALSE; dr=TRUE;
						}
						switch(op) {
							case 'n': // New entry
								{
									if(argc<8) {
										printf("A counter entry must be made..\n");
										return -2;
									}
									//acc_count = atoi(argv[4]);
									if(cr==FALSE) {
										cr = 0;// cr = TRUE; dr = FALSE;
										if(strcmp(argv[7], "cr") != 0) {
											printf("You must add a dr counter entry..\n"); return -2;
										}
									} else if(dr==FALSE) {
										dr = 0;// dr = TRUE; cr = FALSE;
										if(strcmp(argv[7], "dr") != 0) {
											printf("You must add a cr counter entry..\n"); return -2;
										}
									}
									if(argc>10) {
										if(atoi(argv[6]) == atoi(argv[10])) {
											add_entr_(&entr, &acc, id, accid, key, val, cr, dr); // First entry
											accid = atoi(argv[8]);
											key = argv[9];
											val = argv[10];
											++entr_count;
											id = entr_count;
											if(cr == 0) {
												cr = TRUE; dr = 0;
											} else if(dr == 0) {
												dr = TRUE; cr = 0;
											}
											add_entr_(&entr, &acc, id, accid, key, val, cr, dr); // Second entry
										} else {
											printf("Both entries don't match!\n");
										}
									}
									++entr_count;
								}
								break;
							case 'u': // Update entry
								break;
							case 'l': // List entries for selected type
								break;
						}
					} else
						puts("Invalid options given..");
				}
				save_acc_(&acc, acc_count);
				show_entr_(&entr, &acc, entr_count);
			} else if(strcmp(ent, "bals") == 0) { // Balance sheet
				if(op!=0) {
					switch(op) {
						default: // Show balance sheet
							//show_entr_(&entr, &acc, entr_count);
							show_bals_(&acc, &type, acc_count);
							break;
					}
				}
			} else if(strcmp(ent, "incs") == 0) { // Income statement
				if(op!=0) {
					switch(op) {
						default:
							show_incs_(&acc, &type, acc_count);
							break;
					}
				}
			}
		}
	}
	return 0;
}
