/**
* @inventory header file for accs-accounting
* @author VizCreations
*
*/

#define INVT_FILE "inventory.csv"
#define MAX_ITEMS 500000
#define MAX_ITEM_ID 254
#define MAX_ITEM_TITLE 100
#define MAX_ITEM_LEN 255

typedef struct Item {
	char id[MAX_ITEM_ID];
	char comp_id[MAX_ITEM_ID];
	char title[MAX_ITEM_TITLE];
	char desc[MAX_ITEM_LEN];
	int quantity;
	double sprice;
	double oprice;
	double mrp;
	double weight;
	char timestamp[MAX_ITEM_LEN];
} ITM;

void add_item_(ITM *itm, char *);
void del_item_(ITM *itm, int);
