/**
* @account-type header file with macros and constant definitions for accs-accounting
* @author VizCreations
*
*/

#define MAX_TYPE_INPUT 255
#define MIN_TYPE_INPUT 51
#define MAX_TYPE 100
#define MAX_TYPE_MICR 55
#define MAX_TYPE_LINE 200
#define MAX_TYPE_STR 2000

#define TYPE_FILE "types.csv"

typedef struct Type {
	char id[MAX_TYPE_INPUT];
	char name[MAX_TYPE_INPUT];	
	char desc[MAX_TYPE_INPUT];
	char code[MIN_TYPE_INPUT];
	char timestamp[MAX_TYPE_INPUT];
} TYP;

void add_type_(TYP (*type)[], int, char *);
void del_type_(TYP (*type)[], int);
void show_types_(TYP (*type)[], int);
