/**
* @accounting software as a service on the cloud
* @author Webauro
* @abstract Library header file with structures and function declarations
*/

/** This file is part of AccS.

*    AccS is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    AccS is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with AccS.  If not, see <http://www.gnu.org/licenses/>.
*/

#define MAX_STR 2000
#define MAX_INPUT 254
#define MIN_INPUT 21
#define MAX_FILEPATH 200
#define EXTRA 5
#define MAX_ID MIN_INPUT + EXTRA

enum {
	FALSE = -1,
	TRUE = 1
};

typedef short boolean;

#define ACCFILE "accounts.csv"
#define COMPFILE "companies.csv"
#define ENTFILE "entries.csv"
