/**
* @entry header file for accs-accounting
* @author VizCreations
*
*/

#define MAX_ENTR 500
#define MAX_ENTR_ID 254
#define MAX_ENTR_INPUT 255
#define MIN_ENTR_INPUT 51
#define MAX_ENTR_STR 2000
#define ENTR_FILE "entries.csv"
#define JRNL_FILE "journal.csv"

#define MAX_ENTR_LINE 200

typedef struct Entry {
	char id[MAX_ENTR_ID];
	char acc_id[MAX_ENTR_ID];
	char comp_id[MAX_ENTR_ID];
	short cr;
	short dr;
	char name[MAX_ENTR_INPUT];
	int value;
	char code[MIN_ENTR_INPUT];
	char timestamp[MAX_ENTR_INPUT];
} ENTR;

void add_entr_(ENTR (*entr)[], ACC (*acc)[], int, int, char *, char *, short, short);
void del_entr_(ENTR (*entr)[], int); // id in int
void show_entr_(ENTR (*entr)[], ACC (*acc)[], int);
void save_entr_(ENTR (*entr)[], int);
